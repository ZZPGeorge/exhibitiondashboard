import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductionMonitorComponent } from './production-monitor/production-monitor.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/productionData' },
  { path: 'productionData', component: ProductionMonitorComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
