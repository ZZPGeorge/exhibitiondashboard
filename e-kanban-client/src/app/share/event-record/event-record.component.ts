import { Component, OnInit, Input } from '@angular/core';
import { RecordLog } from 'src/app/Model/recordLog.model';

@Component({
  selector: 'app-event-record',
  templateUrl: './event-record.component.html',
  styleUrls: ['./event-record.component.scss']
})
export class EventRecordComponent implements OnInit {

  @Input() records: RecordLog[];
  @Input() infoWidth: number;
  constructor() { }

  ngOnInit() {
  }

}
