import { Component, OnInit, Input } from '@angular/core';
import { CardParams } from 'src/app/Model/cardparams.model';

@Component({
  selector: 'app-card-data',
  templateUrl: './card-data.component.html',
  styleUrls: ['./card-data.component.scss']
})
export class CardDataComponent implements OnInit {

  @Input() params: CardParams[];
  constructor() { }

  ngOnInit() {
  }

}
