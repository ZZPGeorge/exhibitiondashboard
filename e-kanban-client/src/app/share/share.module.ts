import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { BottombarComponent } from './bottombar/bottombar.component';
import { CardDataComponent } from './card-data/card-data.component';
import { BrowserModule } from '@angular/platform-browser';
import { EventRecordComponent } from './event-record/event-record.component';
@NgModule({
    imports: [
        NgZorroAntdModule,
        FormsModule,
        BrowserModule
    ],
    exports: [
        NgZorroAntdModule,
        FormsModule,
        BottombarComponent,
        CardDataComponent,
        EventRecordComponent,
        BrowserModule
    ],
    declarations: [BottombarComponent, CardDataComponent, EventRecordComponent]
})

export class ShareModule {

}
