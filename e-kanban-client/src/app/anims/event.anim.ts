import { trigger, state, transition, style, animate, keyframes, group } from '@angular/animations';

export const slideToRight = trigger('logAnim', [
    transition(':enter', [
        style({ transform: 'translateX(-50%)', opacity: 0 }),
        group([
            animate('0.5s ease-in-out', style({ transform: 'translateX(0)' })),
            animate('.3s ease-in', style({ opacity: 1 }))
        ])


    ]),
  
])