import { Component, OnInit, Input } from '@angular/core';
import { CardParams } from 'src/app/Model/cardparams.model';
declare const G2: any;
declare const DataSet: any;

@Component({
  selector: 'app-power-consume',
  templateUrl: './power-consume.component.html',
  styleUrls: ['./power-consume.component.scss']
})
export class PowerConsumeComponent implements OnInit {

  @Input() paramsInfo: CardParams[];
  @Input() outputData: any;

  chart;
  constructor() {

  }

  ngOnInit() {
   

    this.InitChartData();
  }

  InitChartData() {
    // 898 456
    this.chart = new G2.Chart({
      container: 'powerChart',
      height: 456,
      padding: 'auto',// 为了防止小图时图表变形
      forceFit: true,
    });

    this.chart.source(this.outputData, {
      value: {
        min: 0,
        tickInterval: 30,
        alias: '产能'
      },
      value2: {
        min: 0,
        tickInterval: 30,
        alias: 'output2'
      }
    }
    );
    this.chart.interval().position('time*value').color('#2aad5f');
    this.chart.line().position('time*value2').color('#2aad5f').size(2).shape('smooth');
    this.chart.axis('time', {
      label: {
        formatter: val => {
          return val + 'am'; // 格式化坐标轴显示
        },
        textStyle: {
          fill: 'white'
        }
      }
    });
    this.chart.axis('value', {
      line: null,
      tickLine: null,
      label: {
        textStyle: {
          fill: 'white'
        }
      }
    });
    this.chart.axis('value2', {
      line: null,
      tickLine: null,
      label: {
        textStyle: {
          fill: '#0068B7'
        }
      }
    });
    const margin = 1 / this.outputData.length;
    this.chart.scale('time', {
      range: [margin / 2, 1 - margin / 2]
    });
    this.chart.render();
  }

}
