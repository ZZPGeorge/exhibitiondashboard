import { Component, OnInit, Input } from '@angular/core';
import { CardParams } from 'src/app/Model/cardparams.model';
declare const G2: any;
declare const DataSet: any;

@Component({
  selector: 'app-yield-statistics',
  templateUrl: './yield-statistics.component.html',
  styleUrls: ['./yield-statistics.component.scss']
})
export class YieldStatisticsComponent implements OnInit {
  @Input() paramsInfo: CardParams[];
  @Input() outputData: any;
  chart;
  constructor() { }

  ngOnInit() {
    this.InitChartData();
  }

  InitChartData() {
    // 898 456
    this.chart = new G2.Chart({
      container: 'yieldChart',
      height: 580,
      padding: 'auto',// 为了防止小图时图表变形
      forceFit: true,
    });

    this.chart.source(this.outputData, {
      value: {
        min: 0,
        tickInterval: 30,
        alias: '耗能'
      },
    }
    );
    this.chart.intervalDodge().position('time*value').color('type');
    this.chart.axis('time', {
      label: {
        formatter: val => {
          return val + 'am'; // 格式化坐标轴显示
        },
        textStyle: {
          fill: 'white'
        }
      }
    });
    this.chart.axis('value', {
      line: null,
      tickLine: null,
      label: {
        textStyle: {
          fill: 'white'
        }
      }
    });
    const margin = 1 / this.outputData.length;
    this.chart.scale('time', {
      range: [margin / 2, 1 - margin / 2]
    });
    this.chart.render();
  }



}
