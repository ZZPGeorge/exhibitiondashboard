import { Component, OnInit } from '@angular/core';
import { CardParams } from '../Model/cardparams.model';
import { RecordLog, LogLevel } from '../Model/recordLog.model';

@Component({
  selector: 'app-production-monitor',
  templateUrl: './production-monitor.component.html',
  styleUrls: ['./production-monitor.component.scss']
})
export class ProductionMonitorComponent implements OnInit {
  powerData: CardParams[];
  gasData: CardParams[];
  yieldData: CardParams[];
  powerChartData: any;
  gasChartData: any;
  yieldlChartData: any;
  runTimeChartData: any;
  issueInfos: RecordLog[];
  constructor() {

    this.powerData = [
      {
        key: 'Total',
        value: '22,685.67',
        unit: 'Kwh'
      },
      {
        key: 'Total Running Time',
        value: '20',
        unit: 'H'
      },
      {
        key: 'Total of Power Consumption',
        value: '7.8',
        unit: 'Kwh/ton'
      },
      {
        key: 'Peak Total',
        value: '15,682.55',
        unit: 'Kwh'
      },
      {
        key: 'Valley Total',
        value: '7003.12',
        unit: 'Kwh'
      },
    ];

    this.gasData = [
      {
        key: 'Total',
        value: '7469.88',
        unit: 'm³'
      },
      {
        key: 'Total Running Time',
        value: '20',
        unit: 'H'
      },
      {
        key: 'Total of Gas Consumption',
        value: '2.8',
        unit: 'm³/ton'
      },
      {
        key: 'Peak Total',
        value: '15,682.55',
        unit: 'Kwh'
      },
      {
        key: 'Valley Total',
        value: '7003.12',
        unit: 'Kwh'
      },
    ];

    this.yieldData = [
      {
        key: 'Total',
        value: '7469.88',
        unit: 'Ton'
      },
      {
        key: 'Average',
        value: '20',
        unit: 'Ton/h'
      },
    ];

    this.powerChartData = [
      { time: 1, value: 100, value2: 100 },
      { time: 2, value: 100, value2: 100 },
      { time: 3, value: 100, value2: 100 },
      { time: 4, value: 100, value2: 100 },
      { time: 5, value: 100, value2: 100 },
      { time: 6, value: 100, value2: 100 },
      { time: 7, value: 100, value2: 100 },
      { time: 8, value: 100, value2: 100 },
    ];

    this.gasChartData = [
      { time: '1-2', today: 100, yesterday: 100 },
      { time: '3-4', today: 100, yesterday: 100 },
      { time: '5-6', today: 100, yesterday: 100 },
      { time: '7-8', today: 100, yesterday: 100 },
      { time: '9-10', today: 100, yesterday: 100 },
      { time: '11-12', today: 100, yesterday: 100 },
      { time: '13-14', today: 100, yesterday: 100 },
      { time: '15-16', today: 100, yesterday: 100 },
      { time: '17-18', today: 100, yesterday: 100 },
      { time: '19-20', today: 100, yesterday: 100 },
      { time: '21-22', today: 100, yesterday: 120 },
      { time: '23-24', today: 100, yesterday: 120 },
    ];

    this.yieldlChartData = [
      { time: '1-2', value: 100, type: 'today' },
      { time: '1-2', value: 100, type: 'yesterday' },
      { time: '3-4', value: 100, type: 'today' },
      { time: '3-4', value: 100, type: 'yesterday' },
      { time: '5-6', value: 100, type: 'today' },
      { time: '5-6', value: 100, type: 'yesterday' },
      { time: '7-8', value: 100, type: 'today' },
      { time: '7-8', value: 100, type: 'yesterday' },
      { time: '9-10', value: 100, type: 'today' },
      { time: '9-10', value: 100, type: 'yesterday' },
      { time: '11-12', value: 100, type: 'today' },
      { time: '11-12', value: 100, type: 'yesterday' },
      { time: '13-14', value: 100, type: 'today' },
      { time: '13-14', value: 100, type: 'yesterday' },
      { time: '15-16', value: 100, type: 'today' },
      { time: '15-16', value: 100, type: 'yesterday' },
      { time: '17-18', value: 100, type: 'today' },
      { time: '17-18', value: 100, type: 'yesterday' },
      { time: '19-20', value: 100, type: 'today' },
      { time: '19-20', value: 100, type: 'yesterday' },
      { time: '21-22', value: 100, type: 'today' },
      { time: '21-22', value: 100, type: 'yesterday' },
      { time: '23-24', value: 100, type: 'today' },
      { time: '23-24', value: 100, type: 'yesterday' },
    ];

    this.runTimeChartData = [
      { name: 'G101 Grinder', value: 2, type: 'downtime' },
      { name: 'G101 Grinder', value: 16, type: 'onload' },
      { name: 'G101 Grinder', value: 3, type: 'noload' },
      { name: 'G101 Grinder', value: 2, type: 'for_maintenance' },

      { name: 'G201 Grinder', value: 1.5, type: 'downtime' },
      { name: 'G201 Grinder', value: 16, type: 'onload' },
      { name: 'G201 Grinder', value: 3, type: 'noload' },
      { name: 'G201 Grinder', value: 2, type: 'for_maintenance' },

      { name: 'G301 Grinder', value: 10, type: 'downtime' },
      { name: 'G301 Grinder', value: 6, type: 'onload' },
      { name: 'G301 Grinder', value: 4, type: 'noload' },
      { name: 'G301 Grinder', value: 2, type: 'for_maintenance' },

      { name: 'M103 Mixer', value: 0, type: 'downtime' },
      { name: 'M103 Mixer', value: 1, type: 'onload' },
      { name: 'M103 Mixer', value: 4, type: 'noload' },
      { name: 'M103 Mixer', value: 2, type: 'for_maintenance' },

      { name: 'M203 Mixer', value: 0, type: 'downtime' },
      { name: 'M203 Mixer', value: 18, type: 'onload' },
      { name: 'M203 Mixer', value: 0, type: 'noload' },
      { name: 'M203 Mixer', value: 2, type: 'for_maintenance' },

      { name: 'P105 Pelleter', value: 1, type: 'downtime' },
      { name: 'P105 Pelleter', value: 12, type: 'onload' },
      { name: 'P105 Pelleter', value: 0, type: 'noload' },
      { name: 'P105 Pelleter', value: 2, type: 'for_maintenance' },

      { name: 'E101 Extruder', value: 1.5, type: 'downtime' },
      { name: 'E101 Extruder', value: 18, type: 'onload' },
      { name: 'E101 Extruder', value: 3, type: 'noload' },
      { name: 'E101 Extruder', value: 0, type: 'for_maintenance' },

      { name: 'DR101 Dryer', value: 1, type: 'downtime' },
      { name: 'DR101 Dryer', value: 10, type: 'onload' },
      { name: 'DR101 Dryer', value: 3, type: 'noload' },
      { name: 'DR101 Dryer', value: 1, type: 'for_maintenance' },

      { name: 'SP101 Coater', value: 0, type: 'downtime' },
      { name: 'SP101 Coater', value: 10, type: 'onload' },
      { name: 'SP101 Coater', value: 5, type: 'noload' },
      { name: 'SP101 Coater', value: 1, type: 'for_maintenance' },
    ];

    this.issueInfos = [
      {
        logLevel: LogLevel.success,
        occurTime: new Date(),
        content: 'Mixer M303 starts to execute XXXXXXXX order at '
      },
      {
        logLevel: LogLevel.error,
        occurTime: new Date(),
        content: 'Pelleter P105 maintenance starts at '
      }
    ]


  }

  ngOnInit() {
  }

}
