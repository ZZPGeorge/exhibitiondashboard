import { Component, OnInit, Input } from '@angular/core';
import { CardParams } from 'src/app/Model/cardparams.model';
declare const G2: any;

@Component({
  selector: 'app-gas-consumption',
  templateUrl: './gas-consumption.component.html',
  styleUrls: ['./gas-consumption.component.scss']
})
export class GasConsumptionComponent implements OnInit {
  @Input() paramsInfo: CardParams[];

  @Input() outputData: any;
  chart;
  constructor() { }

  ngOnInit() {
    this.InitChartData();
  }

  InitChartData() {
    // 898 456
    this.chart = new G2.Chart({
      container: 'GasChart',
      height: 456,
      padding: 'auto',// 为了防止小图时图表变形
      forceFit: true,
    });

    this.chart.source(this.outputData, {
      today: {
        min: 0,
        tickInterval: 30,
        alias: '耗能'
      },
      yesterday: {
        min: 0,
        tickInterval: 30,
        alias: '耗能'
      }
    }
    );
    this.chart.line().position('time*today').color('#2aad5f').size(2).shape('smooth');
    this.chart.line().position('time*yesterday').color('#2aad5f').size(2).shape('smooth');
    this.chart.axis('time', {
      label: {
        formatter: val => {
          return val + 'am'; // 格式化坐标轴显示
        },
        textStyle: {
          fill: 'white'
        }
      }
    });
    this.chart.axis('today', {
      line: null,
      tickLine: null,
      label: {
        textStyle: {
          fill: 'white'
        }
      }
    });
    const margin = 1 / this.outputData.length;
    this.chart.scale('time', {
      range: [margin / 2, 1 - margin / 2]
    });
    this.chart.render();
  }


}
