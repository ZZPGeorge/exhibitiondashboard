import { Component, OnInit, Input } from '@angular/core';
import { CardParams } from 'src/app/Model/cardparams.model';
declare const G2: any;
declare const DataSet: any;

@Component({
  selector: 'app-equipment-runtime',
  templateUrl: './equipment-runtime.component.html',
  styleUrls: ['./equipment-runtime.component.scss']
})
export class EquipmentRuntimeComponent implements OnInit {
  @Input() outputData: any;
  chart;
  constructor() { }

  ngOnInit() {
    this.InitChartData();
  }


  InitChartData() {
    // 898 456
    this.chart = new G2.Chart({
      container: 'runTimeChart',
      height: 700,
      padding: 'auto',// 为了防止小图时图表变形
      forceFit: true,
    });

    this.chart.source(this.outputData, {
      value: {
        min: 0,
        tickInterval: 4,
        alias: '时间'
      },
    });
    this.chart.intervalStack().position('name*value').color('type', ['#bdccc9', '#f59a23', '#0075c2', '#13b1b0',]);
    this.chart.axis('name', {
      label: {
        textStyle: {
          fill: 'white'
        }
      }
    });
    this.chart.axis('value', {
      label: {
        textStyle: {
          fill: 'white'
        }
      }
    });
    const margin = 1 / this.outputData.length;
    this.chart.scale('name', {
      range: [margin, 1 - margin]
    });
    this.chart.coord().transpose();
    this.chart.render();
  }
}
