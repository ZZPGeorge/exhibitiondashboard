import { Component, OnInit, Input } from '@angular/core';
import { RecordLog } from 'src/app/Model/recordLog.model';
import { slideToRight } from 'src/app/anims/event.anim';

@Component({
  selector: 'app-key-issues',
  templateUrl: './key-issues.component.html',
  styleUrls: ['./key-issues.component.scss'],
  animations:[
    slideToRight
  ]
})
export class KeyIssuesComponent implements OnInit {

  @Input() eventRecords: RecordLog[];
  
  constructor() { }

  ngOnInit() {
  }

}
