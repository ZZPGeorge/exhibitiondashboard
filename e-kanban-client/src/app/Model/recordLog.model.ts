export interface RecordLog {
    logLevel: LogLevel;
    occurTime: Date;
    content: string;
}

export enum LogLevel {
    success = 1,
    info = 2,
    warn = 3,
    error = 4,
}