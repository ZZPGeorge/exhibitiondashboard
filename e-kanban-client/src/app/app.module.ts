import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import { OperationMonitorComponent } from './operation-monitor/operation-monitor.component';
import { ProductionMonitorComponent } from './production-monitor/production-monitor.component';
import { ShareModule } from './share/share.module';
import { PowerConsumeComponent } from './production-monitor/power-consume/power-consume.component';
import { EquipmentStatusComponent } from './production-monitor/equipment-status/equipment-status.component';
import { GasConsumptionComponent } from './production-monitor/gas-consumption/gas-consumption.component';
import { YieldStatisticsComponent } from './production-monitor/yield-statistics/yield-statistics.component';
import { EquipmentRuntimeComponent } from './production-monitor/equipment-runtime/equipment-runtime.component';
import { KeyIssuesComponent } from './production-monitor/key-issues/key-issues.component';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    OperationMonitorComponent,
    ProductionMonitorComponent,
    PowerConsumeComponent,
    EquipmentStatusComponent,
    GasConsumptionComponent,
    YieldStatisticsComponent,
    EquipmentRuntimeComponent,
    KeyIssuesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ShareModule
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }],
  bootstrap: [AppComponent]
})
export class AppModule { }
